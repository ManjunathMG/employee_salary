<?php

namespace app;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model{

    protected $table = 'emp_salaries';

	protected $primaryKey = 'id';

	protected $fillable = ['id', 'empid', 'empname', 'absent', 'latecome', 'earlycome','working_days','totalsalary','salary'];

    public $timestamps = false;
}
