<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Employee;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;

class IndexController extends Controller
{
    public function addItem(Request $request)
    {
        
        $data = new Employee();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->mobile = $request->mobile;
        $data->designation = $request->designation;
        $data->salary = $request->salary;
        $data->save();
        return response()->json($data);
    }
    public function readItems(Request $req)
    {
        $data = Employee::all();

        return view('welcome')->withData($data);
    }
    public function upload(Request $req)
    {
        return view('upload');
    }
    
public function getDownload() {

        // Sample file is stored under public folder

        $file = "empsalary.xls";

        $headers = array(
            'Content-Type: application/xls',
        );
        return \Response::download($file, "empsalary.xls", $headers);
    }
    public function editItem(Request $req)
    {

        $data = Employee::find($req->id);
        $data->name = $req->name;
        $data->email = $req->email;
        $data->mobile = $req->mobile;
        $data->designation = $req->designation;
        $data->salary = $req->salary;
        $data->save();

        return response()->json($data);
    }

     public function empDatart(Request $request)
    {  
        
        $data_save = new Employee();

        $file = $request->fileupload;
        $path = $request->file('fileupload')->getRealPath();
         $data = \Excel::load($path, function($reader) {})->get();

        foreach ($data as $key => $value) {
           dd($value);
        $data_save->empid = $value->empid;
        $data_save->empname = $value->empname;
        $data_save->absent = $value->absent;
        $data_save->latecome = $value->latecome;
        $data_save->earlycome = $value->earlycome;
        $data_save->salary = $value->totalsalary;
        $data_save->save();     
        } 
      return \Redirect::to('showData');

    }
    public function empData(Request $request)
{
    if($request->hasFile('fileupload')){


        $path = $request->file('fileupload')->getRealPath();
        $data = \Excel::load($path, function($reader) {})->get();

        if(!empty($data) && $data->count()){

            foreach ($data->toArray() as $key => $value) {
               

                if(!empty($value)){
                        $insert[] = ['empid' => $value['empid'], 'empname' => $value['empname'],'absent' => $value['absent'],'working_days' => $value['working_days'],'latecome' => $value['latecome'],'earlycome' => $value['earlycome'],'salary' => $value['salary']];
                               }
            }

            if(!empty($insert)){
                Employee::insert($insert);
                return \Redirect::to('showData');
            }
        }
    }

    return back()->with('error','Please Check your file, Something is wrong there.');

}

    public function showData(Request $request)
    {
       $data = Employee::all();

        return view('welcome')->withData($data);
    }
    public function get_data(Request $request , $empid)
    {

         $user_data = Employee::where('empid', $empid)->first();
         $late_deduct='0';
         $early_credit='0';
         $absent_deduct='0';
         $salary=$user_data['salary'];
         $days=$user_data['working_days'];
         $salary_per_day=$user_data['salary']/$days;
         $absent=$user_data['absent'];
         $early_come=$user_data['earlycome'];
         $late_come=$user_data['latecome'];
         if($absent>0)
         {
            $absent_deduct=$salary_per_day*$absent;
            $salary=$user_data['salary']-$absent_deduct;
         }
         if($late_come>2)
         {
            $salary=$salary-$salary_per_day;
         }
         if($early_come>10)
         {
            $salary=$salary+$salary_per_day;
         }
       
       $salary=round($salary, 2);
        return view('total_salary', [
            'salary' => $salary,
       ]);
        
    }
     public function get_details (Request $request , $empid) {
   
        if (isset($empid)) {

         
            $user_data_admin = Employee::where('empid', $empid)->first();

            if ((!empty($user_data_admin)) ){
                return response()->json(
                    [
                        'status' => 'success',
                        'message' => 'User detail',
                        'success' => $user_data_admin,
                        'statusCode' => 200,
                    ]
                );
            } 

            else {
                return response()->json(
                    [
                        'status' => 'error',
                        'message' => 'User not found',
                        'error' => 'User not found',
                        'statusCode' => 500,
                    ]
                );
            }
        } else {
            return response()->json(
                [
                    'status' => 'error',
                    'message' => 'Email Id required',
                    'success' => 'Email Id required',
                    'statusCode' => 404,
                ]
            );
        }
    }

public function get_data2(Request $request , $empid)
    {
        $data=$this->get_details($request,$empid);
        $data=json_decode($data->getContent(), true);
        $user_data=$data['success'];
        $late_deduct='0';
         $early_credit='0';
         $absent_deduct='0';
         $salary=$user_data['salary'];
         $days=$user_data['working_days'];
         $salary_per_day=$user_data['salary']/$days;
         $absent=$user_data['absent'];
         $early_come=$user_data['earlycome'];
         $late_come=$user_data['latecome'];
         if($absent>0)
         {
            $absent_deduct=$salary_per_day*$absent;
            $salary=$user_data['salary']-$absent_deduct;
         }
         if($late_come>2)
         {
            $salary=$salary-$salary_per_day;
         }
         if($early_come>10)
         {
            $salary=$salary+$salary_per_day;
         }
       
       $salary=round($salary, 2);
        return view('total_salary', [
            'salary' => $salary,
       ]);
                       // return response()->view('total_salary2', compact('user_data_admin'));

    }
}

