<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/master.css') }}">
            </head>
    <body>
	    <br><br><br><br>
      <div class="container">
  		   {{ csrf_field() }}
  		<div class="table-responsive text-center">
  			<table class="table table-borderless" id="table">
  				<thead>
  					<tr>	
  						<th class="text-center">Emp ID</th>
              <th class="text-center">Emp Name</th>
              <th class="text-center">Absent Days</th>
              <th class="text-center">Late come</th>
              <th class="text-center">Earaly Come</th>
              <th class="text-center">Working days</th>
  						<th class="text-center">Salary</th>
              <th class="text-center">Calculate salary</th>
              
  					</tr>
  				</thead>
  				@foreach($data as $item)
  				<tr >
  					<td>{{$item->empid}}</td>
  					<td>{{$item->empname}}</td>
            <td>{{$item->absent}}</td>
            <td>{{$item->latecome}}</td>
            <td>{{$item->earlycome}}</td>
            <td>{{$item->working_days}}</td>
            <td >{{$item->salary }}</td>




            <td><a href="{{ url('api/use_web_service2', ['empid' => $item->empid]) }}" class="btn btn-info">Monthly salary</a></td>

  					

  				</tr>
  				@endforeach
  			</table>
  		</div>
      <div class="center">
            <a href="{{ url('/') }}" class="btn btn-success">
                    <span class="ladda-label">
                        <i class=""></i>
                        Upload file
                    </span>
                </a>
        </div>
  	</div>
		  </div>
      <script src="{{ asset('js/app.js') }}"></script>
      <script src="{{ asset('js/script.js') }}"></script>
    </body>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>

    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.bootstrap.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js" type="text/javascript"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js" type="text/javascript"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js" type="text/javascript"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js" type="text/javascript"></script>
     <script type="text/javascript">
      $('document').ready(function(){
       });
     </script>

</html>
