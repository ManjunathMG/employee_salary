<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <style>
.center {
  margin: auto;
  width: 60%;
 
  padding: 10px;
}
.center2 {
  margin: auto;
  width: 70%;
 
  padding: 60px;
}
</style>
      <!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/master.css') }}">

    </head>
    <body>

	    <br><br><br><br>
        <div class="center">
            <a href="{{ url('/showData') }}" class="btn btn-success">
                    <span class="ladda-label">
                        <i class=""></i>
                        Show Already Stored Data
                    </span>
                </a>
        </div>
      <div class="container">
    		<div class="form-group row add">
    			
    			</div>
           <form method="post" enctype="multipart/form-data" action="{{ url('/empData') }}">
             {{ csrf_field() }}
           <div class="col-md-12 user-form-block personel-detail-block">
                <div class=" col-md-6 form-size" style="padding-left: 0px;" >
                    <div class="form-data">
                        <ul class="list-unstyled">
                            <li class="row personal-data">
                                <div class="col-md-12 personal-profile-pic profile-pic-label">
                                    <!-- <div class="col-md-12" style="padding-left: 0px;padding-top: 50px">
                                    </div> -->
                                    <div class="form-group">
                                        <p>
                                            <label style="font-size: 12px; font-style: italic; ">Upload File in Excel/xlsx formate only*</label>
                                            <input required name="fileupload" class="form-control" placeholder="Choose file" accept=".xlsx ,.csv , .xlsm , .xltx , .xls" data-parsley-file-size-type="data-parsley-file-size-type" id="" type="file" required multiple="multiple">
                                        </p>
                                        <span class="file-path"></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
             <div class="row">
            <div class="col-lg-4">
                <input type="Submit" name="submit" class="btn btn-success" id="filter-btn">
            </div>

<div class="center2">
            <a href="{{ url('/getDownload') }}" class="btn btn-success">
                    <span class="ladda-label">
                        <i class=""></i>
                        Download sample file
                    </span>
                </a>
        </div>
        </div>
        <br>
          </form>
<div class="row pull-left">
            
            </a>
        </div>
             <div class="row pull-left">
            
            </a>
        </div>
    		</div>
  		   
  	
  	</div>
  	<div id="myModal" class="modal fade" role="dialog">
  		











      <script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>
      <script src="{{ asset('js/app.js') }}"></script>
      <script src="{{ asset('js/script.js') }}"></script>
    </body>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">

   

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.bootstrap.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js" type="text/javascript"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js" type="text/javascript"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js" type="text/javascript"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js" type="text/javascript"></script>

</html>
