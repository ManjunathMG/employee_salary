$(document).ready(function() {

  $('#table').DataTable({
    dom: 'Bfrtip',
    buttons: [{
      extend: 'pdf',
      title: 'Customized PDF Title',
      filename: 'customized_pdf_file_name'
    }, {
      extend: 'excel',
      title: 'Customized EXCEL Title',
      filename: 'customized_excel_file_name'
    }, {
      extend: 'csv',
      filename: 'customized_csv_file_name'
    }]
  });

  // $(document).on('click', '.edit-modal', function() {
  //       $('#footer_action_button').text("Update");
  //       $('#footer_action_button').addClass('glyphicon-check');
  //       $('#footer_action_button').removeClass('glyphicon-trash');
  //       $('.actionBtn').addClass('btn-success');
  //       $('.actionBtn').removeClass('btn-danger');
  //       $('.actionBtn').addClass('edit');
  //       $('.modal-title').text('Edit');
  //       $('.deleteContent').hide();
  //       $('.form-horizontal').show();
  //       $('#fid').val($(this).data('id'));
  //       $('#n').val($(this).data('name'));
  //       $('#emailid').val($(this).data('email'));
  //       $('#mobile-number').val($(this).data('mobile'));
  //       $('#designations').val($(this).data('designation'));
  //       $('#salarys').val($(this).data('salary'));
  //       $('#myModal').modal('show');

  //   });
    $(document).on('click', '.delete-modal', function() {
        $('#footer_action_button').text(" Delete");
        $('#footer_action_button').removeClass('glyphicon-check');
        $('#footer_action_button').addClass('glyphicon-trash');
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Delete');
        $('.did').text($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.dname').html($(this).data('name'));
        $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.edit', function() {

        $.ajax({
            type: 'post',
            url: '/editItem',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $("#fid").val(),
                'name': $('#n').val(),
                'email': $('#emailid').val(),
                'mobile': $('#mobile-number').val(),
                'designation': $('#designations').val(),
                'salary': $('#salarys').val(),
            },
            success: function(data) {

                $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.id + "</td> <td>" + data.name + "</td> <td>" + data.email + "</td> <td>" + data.mobile + "</td> <td>" + data.designation + "</td> <td>" + data.salary + "</td> <td> <button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'  data-email='" + data.email + "' data-mobile='" + data.mobile + "'  data-designation='" + data.designation + "'data-salary='" + data.salary + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "'  data-email='" + data.email + "' data-mobile='" + data.mobile + "'  data-designation='" + data.designation + "'data-salary='" + data.salary + "'><span class='glyphicon glyphicon-trash'></span> Delete</button> </td> </tr>");
                 // $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.name + "</td><td>" + data.email + "</td><td>" + data.mobile +"</td><td>" + data.designation + "</td><td>" + data.salary + "</td> <td> <button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'data-email'" + data.email +"' data-mobile'" + data.mobile + "'data-designation'" + data.designation +"' data-salary'" +data.salary+"'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "' ><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                  // $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.name + "</td><td>" + data.email + "</td><td>" + data.mobile +"</td><td>" + data.designation + "</td> <td> <button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "' ><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                 // $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.name + "</td><td>" + data.email + "</td><td>" + data.mobile +"</td><td> <button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "' ><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                 // $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.name + "</td><td>" + data.email + "</td><td> <button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "' ><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                // $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.name + "</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "' ><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
            }
        });
    });
    $("#add").click(function() {
        $.ajax({
            type: 'post',
            url: '/addItem',
            data: {
                '_token': $('input[name=_token]').val(),
                'name': $('input[name=name]').val(),
                'email': $('input[name=email]').val(),
                'mobile': $('input[name=mobile]').val(),
                'designation': $('input[name=designation]').val(),
                'salary': $('input[name=salary]').val(),
            },
            success: function(data) {
                // if ((data.errors)){
                //   $('.error').removeClass('hidden');
                //     $('.error').text(data.errors.name);
                // }
                // else {
                    // $('.error').addClass('hidden');
                    // $('#table').append("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.name + "</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                    $('#table').append("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.name + "</td><td>" + data.mobile + "</td> + <td>" + data.mobile + "</td> + <td>" + data.designation + "</td> + <td>" + data.salary + "</td> <td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                // }
            },

        });
        $('#name').val('');
        $('#email').val('');
        $('#mobile').val('');
        $('#designation').val('');
        $('#salary').val('');
    });
    $('.modal-footer').on('click', '.delete', function() {
        $.ajax({
            type: 'post',
            url: '/deleteItem',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': $('.did').text()
            },
            success: function(data) {
                $('.item' + $('.did').text()).remove();
            }
        });
    });
});
