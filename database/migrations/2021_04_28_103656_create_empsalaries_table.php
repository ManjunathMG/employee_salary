<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpsalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('emp_salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empid');
            $table->string('empname')->nullable();
            $table->string('absent')->nullable();
            $table->string('latecome')->nullable();
            $table->string('earlycome')->nullable(); 
            $table->string('working_days')->nullable(); 
            $table->string('totalsalary')->nullable(); 
            $table->string('salary')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
