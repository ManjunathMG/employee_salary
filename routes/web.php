<?php

Route::get('/', 'IndexController@upload');
Route::post('addItem', 'IndexController@addItem');
Route::post('editItem', 'IndexController@editItem');
Route::get('showData', 'IndexController@showData');
Route::post('empData', 'IndexController@empData');
Route::get('getDownload', 'IndexController@getDownload');
Route::get('use_web_service/{empid}', 'IndexController@get_data');

